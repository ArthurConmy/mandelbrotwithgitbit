#include <QFile>
#include <QTextStream>
#include "complexgrid.h"

// #include "complexnumber.h" // currently, I am unsure of what sort of dependencies
                           // these classes haveon each other. I hoped the stuff in notion
                           // on classes would help, and while I learnt a lot from it, it didn't
                           // have anything about header files

int ComplexGrid::getHeight()
{
    return this->height;
}

int ComplexGrid::getWidth()
{
    return this->width;
}

int ComplexGrid::getIterations()
{
    return this->maxIntensity;
}

int ComplexGrid::no_pixels()
{
    return this->height * this->width;
} // all  these four functions are self-explanatory; return height and width since these are private

int ComplexGrid::no_passing()
{
    int returnValue = 0;

    for(int rowIndex = 0; rowIndex < height; ++rowIndex) {

        for(int colIndex = 0; colIndex < width; ++colIndex) {

            if(grid[rowIndex][colIndex] == maxIntensity) { // in this case, when we called
                                             // ComplexNumber::max_iterations
                                             // the complex number didn't escape
                                             // the circle of radius 2

                returnValue++;
            }
        }
    }

    return returnValue;
}

void ComplexGrid::generate(int newWidth, int newHeight, int maxIterations)
{
    this->width = newWidth;
    this->height = newHeight;
    this->maxIntensity = maxIterations;

    grid.clear(); // if generate is called multiple times, this will be useful

    QVector<int> blankRow(width, 0);
    grid.fill(blankRow, height); // now the grid should be the correct size

    for(int rowIndex = 0; rowIndex < height; ++rowIndex) {
        for(int colIndex = 0; colIndex < width; ++colIndex) {

            ComplexNumber cur_z(-2.0 + (colIndex * 4.0) / (width - 1), 2.0 - (rowIndex * 4.0) / (height - 1));

            // the line above does the faily heavy lifting
            // of evenly dividing up the region -2 < Re(z) < 2, -2 < Im(z) < 2
            // in the complex plane

            grid[rowIndex][colIndex] = cur_z.escape_iterations(maxIterations);
        }
    }
}

void ComplexGrid::exporter(QString filename) // this functions writes
                                             // the complex grid to a file
                                             // using template code from the QT docs
{
    QFile file(filename);
    file.open(QIODevice::ReadWrite);
    QTextStream out(&file);

    out << "P2\n";
    out << width << " " << height << "\n";
    out << maxIntensity << "\n";

    for(int i = 0; i < height; ++i) {
        for(int j = 0; j < width; ++j) {
            out << grid[i][j] << " ";
        }
        out << "\n";
    }

    file.close();
}
