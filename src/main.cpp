#include <QDebug>
#include <iostream>
#include "complexnumber.h"
#include "complexgrid.h"

// NOTE: I have tried to minimise the
// number of #includes throughout and
// am now confused as to how this compiles,
// e.g the QVector testCases doesn't cause a
// compile error but surely QVectors aren't
// defined in QDebug or iostream, and in
// both my header files they aren't included
// either.

int main(int argc, char *argv[]) {

    Q_UNUSED(argc);
    Q_UNUSED(argv); // ignore compiler warnings; from Cadaques section 2.3.1

    ComplexGrid myGrid;

    QVector<QVector<int>> testCases = {{10, 10, 16, 10},
                                       {10, 10, 255, 8},
                                       {32, 16, 100, 42},
                                       {100, 100, 200, 932},
                                       {100, 100, 1000, 914},
                                       {200, 200, 1000, 3732},
                                       {200, 200, 64, 3886}};

    // this is all the test data from the test data section at http://www-h.eng.cam.ac.uk/help/languages/C++/1BC++/mandelbrot.php ,
    // in format {width,height,max iter.,# pass}

    int no_tests_passed = 0;
    bool passedTests = false;

    qWarning() << "Beginning to test ComplexGrid generation against the CUED test cases:\n";

    for(QVector<int> testCase : testCases) {

        myGrid.generate(testCase[0], testCase[1], testCase[2]);

        qWarning() << myGrid.no_passing() << "should be" << testCase[3];

        if(myGrid.no_passing() == testCase[3]) {
            no_tests_passed++;
        }
    }

    if(no_tests_passed == (int)testCases.size()) {
        passedTests=true;
    }

    if(passedTests) {
        qWarning() << "Currently passing all tests";
    }

    else {
        qWarning() << "UH-OH, WE'RE CURRENTLY FAILING SOME TESTS";
    }

    qWarning() << "\n Please enter, in space/line separated format, the width,\n"
               << "height and max number of iterations (used before a point\n"
               << "is determined to not be in the Mandelbrot set).\n\n"
               << "It is recommended to use equal width and height value for image\n"
               << "quality purposes (otherwise the image will not resemble\n"
               << "the Mandelbrot set since it will be squished). In addition,\n"
               << "if the max number of iterations is more than 256, the program\n"
               << "run slower and likely produce a less clear image:\n";

    int width, height, max_iterations;
    std::cin >> width >> height >> max_iterations;

    myGrid.generate(width, height, max_iterations);

    qWarning() << "\n Grid generated. Out of a total of" << myGrid.no_pixels() << "complex\n"
               << "numbers sampled," << myGrid.no_passing() << "did not escape the disk of radius\n"
               << "two after" << myGrid.getIterations() << "iterations. If you'd like to export this\n"
               << "image to a file, enter the file name you'd like it to be\n"
               << "saved as (do NOT include .pgm or any other file extension:\n";

    QTextStream qtin(stdin);
    QString line = qtin.readLine();
    QString filename;
    qtin >> filename;    // I ripped this way of reading QStrings from
                         // the top reply here:
                         // https://stackoverflow.com/questions/2321880/is-it-possible-to-use-cin-with-qt

    myGrid.exporter(filename + ".pgm");

    qWarning() << "\n Successfully exported. Finished : ) ";

    return 0;
}
