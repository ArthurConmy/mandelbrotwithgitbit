#ifndef COMPLEXNUMBER_H
#define COMPLEXNUMBER_H

class ComplexNumber {

public:
    ComplexNumber(double realPart, double imagPart);

    void print();                                       // convenience/debugging function that prints out the given complex number

    double magnitude();                                 // gets the magnitude of a complex number

    int escape_iterations(int);                         // returns the number of escape iterations for the magnitude of a complex number to be
                                                        // greater 2 under the mandelbrot recursion procedure, or returns the given integer
                                                        // passed to escape iterations if after this many iterations still its magnitude is at most 2

    ComplexNumber operator+              // note overloading the operator + now replaces the old, clunky add member function
    (const ComplexNumber& curAdding);

    ComplexNumber operator*              // note overloading the operator * now replaces the old, clunky multiply member function
    (const ComplexNumber& curTimesing);

private:
    double real;                                         // imaginary part of complex number
    double imag;                                         // real part of complex number

                                                         // hmm that these are private could be annoying if I was to come back to this as
};

#endif // COMPLEXNUMBER_H
