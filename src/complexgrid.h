#ifndef COMPLEXGRID_H
#define COMPLEXGRID_H

#include "complexnumber.h" // since we have
                           // ComplexNumbers
                           // here, we should
                           // include the header file

#include <QObject>

class ComplexGrid {

public:
    int getHeight();
    int getWidth();
    int getIterations();
    int no_pixels();

    void generate(int curWidth, int curHeight, int curMaxIterations);
    void exporter(QString filename);                                   // exports the grid to a .pmg file with filename the
                                                                       // given QString (should include .pmg in it)
    int no_passing(); // returns the number of complex numbers
                      // in the grid which don't escape the disk
                      // of radius 2 in maxIntensity iterations
                      // of the Mandelbrot recursion
private:
    int width;
    int height;
    int maxIntensity;
    QVector<QVector<int>> grid;    // note how the grid itself is stored
                                   // (with a 2D QVector for variable size)
};

#endif // COMPLEXGRID_H
