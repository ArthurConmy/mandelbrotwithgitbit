#include <QDebug>
#include "complexnumber.h"

ComplexNumber::ComplexNumber(double realPart, double imagPart)
{
    this->real = realPart;
    this->imag = imagPart;
}

void ComplexNumber::print()
{
    qWarning() << real << "+" << imag << "j";
}

double ComplexNumber::magnitude()
{
    return sqrt((this->real * this->real) + (this->imag * this->imag));
}

ComplexNumber ComplexNumber::operator+ (const ComplexNumber& curAdding)
{
    ComplexNumber temp(0.0,0.0);
    temp.real = real + curAdding.real;
    temp.imag = imag + curAdding.imag;
    return temp;
}

ComplexNumber ComplexNumber::operator* (const ComplexNumber& curTimesing)
{
    ComplexNumber temp(0.0, 0.0);
    temp.real = (this->real * curTimesing.real) - (this->imag * curTimesing.imag);
    temp.imag = (this->real * curTimesing.imag) + (this->imag * curTimesing.real);
    return temp;
}

int ComplexNumber::escape_iterations(int max_iterations)
{
    ComplexNumber x(0.0, 0.0);

    for(int iterationNo = 1; iterationNo <= max_iterations; ++iterationNo) {

        x = (x * x) + *this; // Mandlebrot iterate

        if(x.magnitude() > 2.0) { // Is |x| > 2 ?
            return iterationNo;
        }
    }

    return max_iterations; // |x_max_iterations| <= 2.0 still, so return max_iterations value
}
